﻿using System;

namespace ConsoleApp16
{
    class Program
    {
        static void Main(string[] args)
        {
            Student[] s = new Student[5];

            s[0] = new Student("Gevorg" , "Zamharyan" , 19 , "Armenia" , "Yerevan"); 
            s[1] = new Student("Nshan" , "Simonyan" , 18 , "Armenia" , "Yerevan"); 
            s[2] = new Student("Alex" , "Hovhannisyan" , 19 , "Armenia" , "Yerevan");  
            s[3] = new Student("Suren" , "Harutyunyan" , 20 , "Armenia" , "Yerevan");  
            s[4] = new Student("Artyom" , "Sirekanyan" , 20 , "Armenia" , "Gyumri");


            foreach (var item in s) 
            { 
                item.GetInfo(); 
            }
        } 
    }
}
