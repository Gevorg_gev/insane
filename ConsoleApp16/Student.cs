﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp16
{
    public class Student
    {
        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private string _Surname;
        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; } 
        }

        private int _Age;
        public int Age
        {
            get { return _Age; }
            set { _Age = value; } 
        }

        private string _Country;
        public string Country
        {
            get { return _Country; }
            set { _Country = value; } 
        }

        private string _City;
        public string City
        {
            get { return _City; }
            set { _City = value; } 
        }

        public Student(string _Name, string _Surname, int _Age, string _Country, string _City) 
        {
            this._Name = _Name;
            this._Surname = _Surname;
            this._Age = _Age;
            this.Country = _Country;
            this._City = City; 
        }

        public void GetInfo()
        {
            Console.WriteLine($"Name: {_Name}" +
                $" Surname: {_Surname}" +
                $" Age: {_Age}" +
                $" Country: {_Country}" +
                $" City: {_City}"); 
        }
    }
}
